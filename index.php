<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ma page avec fausses données</title>
</head>
<body>
    <?php require_once 'vendor/autoload.php';
    $loader = new \Twig\Loader\FilesystemLoader(__DIR__."/templates");
    $twig = new \Twig\Environment($loader, [
        'cache' => false,
    ]);
    $faker = Faker\Factory::create('fr_FR');
    // $nom = return($faker->name); 
    // $nom =  $faker->name;
    // $adresse = $faker->streetAddress;
    // $mail = $faker->freeEmail;
    // $tel = $faker->phoneNumber;
    // $company = $faker->company;
    // $job = $faker->jobTitle;
    // $color = $faker->safeColorName;
    // $url = $faker->url;
    // $productMaterial = $faker->randomElement($array = array ('Pierre','Brique','Ciment','Bois','Marbre','Béton'));
    // $prix = $faker->numberBetween($min = 1, $max = 3000)."$";
    // $productName = $faker->randomElement($array = array ('Statue gravée','Mur rouge','Parpaing','Table en fût de chêne','Escalier','Chaise'));
    // $productAdj = $faker->randomElement($array = array ('Géant','Lourd','Beau','Génialissime','Etonnant','Fabuleux'));
    $fortpl = [
        'nom' => $faker->name, //
        'adresse' => $faker->streetAddress, //
        'job' => $faker->jobTitle, //
        'mail' => $faker->freeEmail, //
        'tel' => $faker->phoneNumber, //
        'company-name' => $faker->company, //
        'productName' => $faker->randomElement($array = array ('Statue gravée','Mur rouge','Parpaing','Table en fût de chêne','Escalier','Chaise')), //
        'price' => $faker->numberBetween($min = 1, $max = 3000)."$", //
        'color' => $faker->safeColorName, //
        'productMaterial' => $faker->randomElement($array = array ('Pierre','Brique','Ciment','Bois','Marbre','Béton')), //
        'url' => $faker->url, //
        'productAdjective' => $faker->randomElement($array = array ('Géant','Lourd','Beau','Génialissime','Etonnant','Fabuleux')) //
        ];
    // $template = $twig->load('index.html');
    echo $twig->render('index.html', $fortpl);
    ?>

</body>
</html>